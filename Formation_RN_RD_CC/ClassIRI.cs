﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formation_RN_RD_CC
{
    public class ClassIRI
    {
        // ID IRI
        public int ID = 0;

        // FreqKHz
        public double F_IRI = 0;

        public double Latitude;
        public double Longitude;

        // Относительные характеристики СП_ИРИ
        // !!! Только для СП, к-ые видят этот ИРИ
        public List<SP_IRI> lst_SP_IRI = new List<SP_IRI>();

    } // Class
} // Namespace
