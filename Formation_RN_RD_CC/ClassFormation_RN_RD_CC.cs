﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ModelsTablesDBLib;
using Bearing;

namespace Formation_RN_RD_CC
{

    public static class ClassFormation_RN_RD_CC
    {

        // Формирование радиосетей: F==,XYZ!= (радионаправление==РС с двумя ИРИ)
        public static List<SRNet> Organization_RNet(

                                        // СП
                                        //List<TableASP> tableAsp,
                                        // ИРИ
                                        List<TableReconFWS> IRI_RNet_Comm,

                                       // !!! Пока внутри программы dF=3KHz dPel=2grad dD=500m
                                       double dD, // m
                                       double dF, //KHz
                                       double dPel // grad

                                       //ref List<SRNet> list_RNet
                                       )
        {
            // ---------------------------------------------------------------------------------------
            int nIRI_all = 0;
            int ind1 = 0;
            int shIRI = 0;
            int shSP = 0;
            int NumbRNet = 0;
            int NumbRNet1 = 0;
            int flRNet_Coord = 0;
            int ind_first = 0;
            int fl_first_Net = 0;
            int ind_second = 0;
            double dx = 0;
            double dy = 0;
            double D = 0;
            int i_tmp = 0;
            int i_tmp1 = 0;
            int fl_tmp = 0;
            // ---------------------------------------------------------------------------------------
            // Кол-во ИРИ

            nIRI_all = IRI_RNet_Comm.Count;
            // ---------------------------------------------------------------------------------------
            // Переформированный входной массив ИРИ
            List<ClassIRI> list_IRI_RNet = new List<ClassIRI>();

            // Для формирования радиосетей
            List<SRNet> list_SRNet = new List<SRNet>();
            List<SRNet> list_SRNet1 = new List<SRNet>();

            // Опорный
            //ClassIRI objS_IRI_RNet3 = new ClassIRI();
            //objS_IRI_RNet3.lst_SP_IRI = new List<SP_IRI>();
            // Second
            //ClassIRI objS_IRI_RNet2 = new ClassIRI();
            //objS_IRI_RNet2.lst_SP_IRI = new List<SP_IRI>();

            ClassIRI objS_IRI = new ClassIRI();

            // i-я РС-ть
            //SRNet objSRNet = new SRNet();
            //objSRNet.list_IRI_ID = new List<ClassIRI>();

            // ---------------------------------------------------------------------------------------
            dF = 3; // KHz
            dPel = 2; // grad
            dD = 500; // m
            // ---------------------------------------------------------------------------------------

            // IRI **********************************************************************************
            // Формирование list_IRI_RNet

            double levelmin = -140;
            double levelmax = 0;

            // FOR**
            for (shIRI = 0; shIRI < nIRI_all; shIRI++)
            {

                // -----------------------------------------------------------------------------------
                ClassIRI objS_IRI_RNet = new ClassIRI();
                objS_IRI_RNet.lst_SP_IRI = new List<SP_IRI>();
                // -----------------------------------------------------------------------------------
                // ID

                objS_IRI_RNet.ID = IRI_RNet_Comm[shIRI].Id;
                // -----------------------------------------------------------------------------------
                // Freq

                objS_IRI_RNet.F_IRI = (double)IRI_RNet_Comm[shIRI].FreqKHz;
                // -----------------------------------------------------------------------------------
                // Coordinates

                objS_IRI_RNet.Latitude = IRI_RNet_Comm[shIRI].Coordinates.Latitude;
                objS_IRI_RNet.Longitude = IRI_RNet_Comm[shIRI].Coordinates.Longitude;
                // -----------------------------------------------------------------------------------

                // lst_SP_IRI >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // Составляем список СП, которые видят этот ИРИ

                // FOR12
                for (shSP = 0; shSP < IRI_RNet_Comm[shIRI].ListJamDirect.Count; shSP++)
                {
                    // ..............................................................................
                    SP_IRI objSP_IRI = new SP_IRI();
                    // ..............................................................................

                    // Эта СП видит этот ИРИ
                    if ((IRI_RNet_Comm[shIRI].ListJamDirect[shSP].JamDirect.Level >= levelmin) &&
                        (IRI_RNet_Comm[shIRI].ListJamDirect[shSP].JamDirect.Level <= levelmax))
                    {
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        // Номер СП

                        objSP_IRI.N_SP = IRI_RNet_Comm[shIRI].ListJamDirect[shSP].JamDirect.NumberASP;
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        // Пеленг от этой станции/-1

                        objSP_IRI.Bearing = IRI_RNet_Comm[shIRI].ListJamDirect[shSP].JamDirect.Bearing;
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        objS_IRI_RNet.lst_SP_IRI.Add(objSP_IRI);
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    } // IF(Эта СП видит этот ИРИ)
                    // ..............................................................................

                } // FOR12 -> по списку СП в этом ИРИ

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> lst_SP_IRI

                list_IRI_RNet.Add(objS_IRI_RNet);

            } // FOR** -> IRI
              // ********************************************************************************** IRI

            // RNet_XY ******************************************************************************
            // Выделение радиосетей и радионаправлений для ИРИ с координатами
            // F==; XY!=

            flRNet_Coord = 0;  // Нашли опорный
            ind_first = 0;     // Индекс опорного ИРИ
            ind_second = 0;    // Следующий за опорным
            fl_first_Net = 0;  // Словлена РС (т.е кроме опорного, нашли второй)

            // WHILE1 (индекс ИРИ, с которым сравниваем остаток массива)
            // Опорный еще в пределах листа
            while (ind_first < list_IRI_RNet.Count)
            {

                // OPORN -------------------------------------------------------------------------------
                // Поиск опорного ИРИ

                // FOR1
                for (ind1 = ind_first; ind1 < list_IRI_RNet.Count; ind1++)
                {
                    if (
                        (list_IRI_RNet[ind1].Latitude != 0) &&
                        (list_IRI_RNet[ind1].Longitude != 0) &&
                        (list_IRI_RNet[ind1].Latitude != -1) &&
                        (list_IRI_RNet[ind1].Longitude != -1) &&
                        (flRNet_Coord == 0)
                        )
                    {
                        // Это опорный
                        flRNet_Coord = 1;
                        ind_first = ind1;

                        // Фиксируем опорный
                        ClassIRI objS_IRI_RNet3 = new ClassIRI();
                        objS_IRI_RNet3.lst_SP_IRI = new List<SP_IRI>();
                        objS_IRI_RNet3.ID = list_IRI_RNet[ind1].ID;
                        objS_IRI_RNet3.F_IRI = list_IRI_RNet[ind1].F_IRI;
                        objS_IRI_RNet3.Latitude = list_IRI_RNet[ind1].Latitude;
                        objS_IRI_RNet3.Longitude = list_IRI_RNet[ind1].Longitude;
                        objS_IRI_RNet3.lst_SP_IRI = list_IRI_RNet[ind1].lst_SP_IRI;


                        SRNet objSRNet = new SRNet();
                        objSRNet.list_IRI_ID = new List<ClassIRI>();

                        // Добавляем в РС, НО из основного листа пока не убираем
                        NumbRNet += 1; // N RS

                        objSRNet.NRNet = NumbRNet;
                        objSRNet.list_IRI_ID.Add(objS_IRI_RNet3);
                        list_SRNet.Add(objSRNet);

                        // Выходим из FOR 
                        ind1 = list_IRI_RNet.Count + 1;

                    } // нашли 1-й с координатами


                } // FOR1 (опорный: индекс ИРИ, с которым сравниваем остаток массива)
                  // ------------------------------------------------------------------------------- OPORN

                // NO_OPORN_COORD -----------------------------------------------------------------------
                // Не нашли опорного с координатами

                // IF1
                if (flRNet_Coord == 0)
                {
                    // Выходим из WHILE1 (из поиска по координатам)
                    ind_first = list_IRI_RNet.Count + 1;

                } // IF1
                  // ----------------------------------------------------------------------- NO_OPORN_COORD

                // FIND_OPORN_COORD ---------------------------------------------------------------------
                // Опорный с координатами был найден

                // ELSE on IF1
                else
                {

                    ind_second = ind_first + 1;

                    // SECOND >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Проверка остатка массива на вопрос присоединения к опорному ИРИ

                    // WHILE2
                    while (ind_second < list_IRI_RNet.Count)
                    {

                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        // IF** есть координаты
                        if (
                            (list_IRI_RNet[ind_second].Latitude != 0) &&
                            (list_IRI_RNet[ind_second].Longitude != 0) &&
                            (list_IRI_RNet[ind_second].Latitude != -1) &&
                            (list_IRI_RNet[ind_second].Longitude != -1)
                            )
                        {

                            // Фиксируем Second
                            ClassIRI objS_IRI_RNet2 = new ClassIRI();
                            objS_IRI_RNet2.lst_SP_IRI = new List<SP_IRI>();
                            objS_IRI_RNet2.ID = list_IRI_RNet[ind_second].ID;
                            objS_IRI_RNet2.F_IRI = list_IRI_RNet[ind_second].F_IRI;
                            objS_IRI_RNet2.Latitude = list_IRI_RNet[ind_second].Latitude;
                            objS_IRI_RNet2.Longitude = list_IRI_RNet[ind_second].Longitude;
                            objS_IRI_RNet2.lst_SP_IRI = list_IRI_RNet[ind_second].lst_SP_IRI;

                            if (fl_first_Net == 0)
                            {
                                D = ClassBearing.f_D_2Points(
                                                            list_IRI_RNet[ind_first].Latitude,
                                                            list_IRI_RNet[ind_first].Longitude,
                                                            objS_IRI_RNet2.Latitude,
                                                            objS_IRI_RNet2.Longitude,
                                                            1);
                            }
                            else
                            {
                                if (NumbRNet != 0)
                                {
                                    D = ClassBearing.f_D_2Points(
                                                                list_SRNet[NumbRNet - 1].list_IRI_ID[0].Latitude,
                                                                list_SRNet[NumbRNet - 1].list_IRI_ID[0].Longitude,
                                                                objS_IRI_RNet2.Latitude,
                                                                objS_IRI_RNet2.Longitude,
                                                                1);
                                }
                            }

                            // ...............................................................
                            // Подходит для РС

                            // IF3
                            if (
                                ((fl_first_Net == 0) && (D > dD) && (Math.Abs(list_IRI_RNet[ind_first].F_IRI - objS_IRI_RNet2.F_IRI) < dF)) ||
                                ((NumbRNet != 0) && (fl_first_Net == 1) && (D > dD) && (Math.Abs(list_SRNet[NumbRNet - 1].list_IRI_ID[0].F_IRI - objS_IRI_RNet2.F_IRI) < dF))
                                )
                            {


                                // Формируем РС #############################################################
                                // Формируем РС

                                // """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                                // 1-й, кроме опорного, в РС

                                // IF4
                                if (fl_first_Net == 0)
                                {
                                    fl_first_Net = 1;

                                    // Добавляем в РС (!!! опорный уже добавлен)
                                    if (NumbRNet != 0)
                                    {
                                        list_SRNet[NumbRNet - 1].list_IRI_ID.Add(objS_IRI_RNet2);
                                    }

                                    // Убрать этот ИРИ из основного списка
                                    list_IRI_RNet.RemoveAt(ind_second);
                                    // Убрать опорный
                                    list_IRI_RNet.RemoveAt(ind_first);
                                    ind_second -= 1; // Т.к. убрали сразу два

                                } // IF4 Формируем РС
                                // """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                                // Добавляем ИРИ к РС

                                else // On IF4
                                {
                                    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    // Проверяем, не расположен ли он близко к уже имеющимся в РС

                                    fl_tmp = 0;

                                    if (NumbRNet != 0)
                                    {
                                        for (i_tmp = 0; i_tmp < list_SRNet[NumbRNet - 1].list_IRI_ID.Count; i_tmp++)
                                        {
                                            D = ClassBearing.f_D_2Points(list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].Latitude,
                                                                          list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].Longitude,
                                                                          objS_IRI_RNet2.Latitude,
                                                                          objS_IRI_RNet2.Longitude,
                                                                          1);

                                            if (D < dD)
                                            {
                                                fl_tmp = 1;
                                                i_tmp = list_SRNet[NumbRNet - 1].list_IRI_ID.Count + 1; // Out fromFOR
                                            }
                                        }
                                    }
                                    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    // Добавляем в РС

                                    // IF6
                                    if (fl_tmp == 0)
                                    {
                                        // Добавляем в РС (!!! опорный уже добавлен)
                                        if (NumbRNet != 0)
                                        {
                                            list_SRNet[NumbRNet - 1].list_IRI_ID.Add(objS_IRI_RNet2);
                                        }

                                        // Убрать этот ИРИ из основного списка
                                        list_IRI_RNet.RemoveAt(ind_second);

                                    } // IF6
                                      // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                      // Если этот ИРИ стоял близко к какому-то из ИРИ в этой РС

                                    else  // On IF6
                                    {
                                        // Убрать этот ИРИ из основного списка
                                        list_IRI_RNet.RemoveAt(ind_second);

                                    }
                                    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                                } // else onIF4
                                // """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

                                // ############################################################# Формируем РС

                            } // IF3 (Подходит для РС)
                            // ...............................................................
                            // Не подходит для РС -> пропускаем

                            // ELSE on IF3
                            else
                            {

                                if (
                                    ((fl_first_Net == 0) && (D <= dD) && (Math.Abs(list_IRI_RNet[ind_first].F_IRI - objS_IRI_RNet2.F_IRI) < dF)) ||
                                    ((NumbRNet != 0) && (fl_first_Net == 1) && (D <= dD) && (Math.Abs(list_SRNet[NumbRNet - 1].list_IRI_ID[0].F_IRI - objS_IRI_RNet2.F_IRI) < dF))
                                    )
                                {
                                    // Убрать этот ИРИ из основного списка
                                    list_IRI_RNet.RemoveAt(ind_second);
                                }
                                else
                                    ind_second += 1;

                            } // ELSE on IF3
                            // ...............................................................


                        } // IF** есть координаты
                          // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                          // ELSE on IF** -> пропускаем (нет координат)

                        else
                        {
                            ind_second += 1;

                        } // else  on if** (no coordinates)
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    } // WHILE2 (Find second)
                      // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> SECOND

                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Проверяем, сформировалась ли РС
                    // РС сформировалась, т.е. опорный убрали -> переходим к следующему, но индекс тот же
                    // т.к. массив сдвинулся

                    // РС не сформировалась -> Переходим к следующему опорному
                    if (fl_first_Net == 0)
                    {
                        ind_first += 1;
                        flRNet_Coord = 0;
                        if (NumbRNet != 0)
                        {
                            list_SRNet.RemoveAt(NumbRNet - 1);
                            NumbRNet -= 1;
                        }
                        ind_second = ind_first + 1;
                        fl_first_Net = 0; // Словлена РС

                    } // РС не сформировалась

                    else
                    {
                        flRNet_Coord = 0;
                        ind_second = ind_first + 1;
                        fl_first_Net = 0; // Словлена РС
                    }
                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""


                }  // ELSE on IF1 (Опорный с координатами был найден)
                   // --------------------------------------------------------------------- FIND_OPORN_COORD

            } // WHILE1 (ind_first < list_IRI_RNet.Count)
              // ****************************************************************************** RNet_XY


            // RNet_Pel ******************************************************************************
            // Выделение радиосетей и радионаправлений по ИРИ без координат
            // (F==; Pel!=)

            // 11111111111111111111111111111111111111111111111111111111111
            // Сначала проходимся по уже сформированным РС и проверяем, если
            // частото и пеленг совпадают с ... ИРИ в РС-> убираем.
            // Если частоа==, а пеленги != -> добавляем в эту РС

            int fl_add = 0;
            int i_tmp2 = 0;
            int fl_compare = 0;

            ind1 = 0;
            // WHILE20 -> on IRI main list
            while (ind1 < list_IRI_RNet.Count)
            {
                // ------------------------------------------                
                // IF20 -> No Coordinates
                if (
                    ((list_IRI_RNet[ind1].Latitude == 0) || (list_IRI_RNet[ind1].Longitude == 0)) ||
                    ((list_IRI_RNet[ind1].Latitude == -1) || (list_IRI_RNet[ind1].Longitude == -1))
                    )
                {
                    // ......................................
                    // Проходим по РС-ям

                    fl_tmp = 0;
                    fl_add = 0;

                    // FOR20 -> RS
                    for (i_tmp = 0; i_tmp < list_SRNet.Count; i_tmp++)
                    {
                        // ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                        // FOR21 IRI in RSi
                        for (i_tmp1 = 0; i_tmp1 < list_SRNet[i_tmp].list_IRI_ID.Count; i_tmp1++)
                        {
                            // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            // Подходит по частоте, но входит в этот же пеленг

                            // IF22
                            if (
                                //(
                                //(Math.Abs(list_IRI_RNet[ind1].Pel1 - list_SRNet[i_tmp].list_IRI_ID[i_tmp1].Pel1) < dPel) ||
                                //(Math.Abs(list_IRI_RNet[ind1].Pel1 - list_SRNet[i_tmp].list_IRI_ID[i_tmp1].Pel2) < dPel)
                                //) &&
                                (Math.Abs(list_IRI_RNet[ind1].F_IRI - list_SRNet[i_tmp].list_IRI_ID[i_tmp1].F_IRI) < dF)
                                )
                            {
                                fl_compare = ComparePeleng(list_IRI_RNet[ind1], list_SRNet[i_tmp].list_IRI_ID[i_tmp1], dPel);

                                // Пеленги по одинаковой станции совпали
                                if (fl_compare == 1)
                                {
                                    // Del
                                    fl_tmp = 1;
                                    i_tmp1 = list_SRNet[i_tmp].list_IRI_ID.Count; // Out from FOR21
                                }

                                else
                                {
                                    // 555
                                    if ((i_tmp1 == 0) && (fl_compare != -2))
                                    {
                                        fl_add = 1;
                                    }
                                }

                            } // IF22
                            // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            // Подходит по частоте, пеленги не равны->
                            // Добавляем в эту РС, убираем из основного списка

                            // else on IF22 Add to RS
                            //else if (
                            // (Math.Abs(list_IRI_RNet[ind1].F - list_SRNet[i_tmp].list_IRI_ID[i_tmp1].F) < dF) &&
                            // (Math.Abs(list_IRI_RNet[ind1].Pel1 - list_SRNet[i_tmp].list_IRI_ID[i_tmp1].Pel1) >= dPel) &&
                            // (Math.Abs(list_IRI_RNet[ind1].Pel1 - list_SRNet[i_tmp].list_IRI_ID[i_tmp1].Pel2) >= dPel) &&
                            // (i_tmp1 == 0)
                            //       )
                            //{
                            //    fl_add = 1;

                            //} // else IF22 Add to RS
                            // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                        } // FOR21 IRI in RSi
                        // ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                        // DelIRI

                        // IF23
                        if (fl_tmp == 1)
                        {
                            fl_add = 0;

                            //fl_tmp = 0;
                            list_IRI_RNet.RemoveAt(ind1);
                            //!!! ind1 no change
                            i_tmp = list_SRNet.Count; // Out from FOR20

                        } // IF23
                        // ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                        // Add to RSi

                        // IF24
                        if (fl_add == 1)
                        {

                            // Фиксируем IRI
                            ClassIRI objS_IRI_RNet5 = new ClassIRI();
                            objS_IRI_RNet5.lst_SP_IRI = new List<SP_IRI>();
                            objS_IRI_RNet5.ID = list_IRI_RNet[ind1].ID;
                            objS_IRI_RNet5.F_IRI = list_IRI_RNet[ind1].F_IRI;
                            objS_IRI_RNet5.Latitude = list_IRI_RNet[ind1].Latitude;
                            objS_IRI_RNet5.Longitude = list_IRI_RNet[ind1].Longitude;
                            objS_IRI_RNet5.lst_SP_IRI = list_IRI_RNet[ind1].lst_SP_IRI;
                            // Добавляем в RSi
                            list_SRNet[i_tmp].list_IRI_ID.Add(objS_IRI_RNet5);

                            // Убрать этот ИРИ из основного списка
                            list_IRI_RNet.RemoveAt(ind1);

                            //!!! ind1 no change
                            i_tmp = list_SRNet.Count; // Out from FOR20

                        } // IF24 AddtoRS
                        // ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


                    } // FOR20 Проходим по РС-ям
                    // ......................................

                    if ((fl_tmp == 1) || (fl_add == 1)) // DelIRI/Add IRI
                    {
                        fl_tmp = 0; //!!! ind1 no change
                        fl_add = 0;
                    }
                    else ind1 += 1;

                } // IF20 NO coordinates
                // ------------------------------------------                
                // Есть координаты -> Пропускаем

                else // On IF20 (No coordinates)
                {
                    ind1 += 1;
                }
                // ------------------------------------------                

            } // WHILLE20  list_IRI_RNet
              // 11111111111111111111111111111111111111111111111111111111111

            // 22222222222222222222222222222222222222222222222222222222222
            // Из оставшихся формируем РС по пеленгу(это м.б. и оставшиеся с координатами)

            double dp1 = 0;
            double dp2 = 0;
            int fp = 0;
            int fp_1 = 0;
            double dff = 0;
            int fcompp = 0;
            int fcompp1 = 0;

            // ------------------------------------------------------------

            flRNet_Coord = 0;  // Нашли опорный
            ind_first = 0;     // Индекс опорного ИРИ
            ind_second = 0;    // Следующий за опорным
            fl_first_Net = 0;  // Словлена РС (т.е кроме опорного, нашли второй)

            // WHILE1 (опорный еще в пределах основного листа) 
            while (ind_first < list_IRI_RNet.Count)
            {

                // -------------------------------------------------------
                // Для поиска опорного ири в РС

                // FOR1
                for (ind1 = ind_first; ind1 < list_IRI_RNet.Count; ind1++)
                {
                    //if ((list_IRI_RNet[ind1].X != 0) && (list_IRI_RNet[ind1].Y != 0) && (flRNet_Coord == 0))
                    // Здесь берем просто 1-й в списке

                    // 555
                    if ((flRNet_Coord == 0) && // Еще не было опорного
                        (TestPeleng(list_IRI_RNet[ind1]) == 1)
                       )
                    {
                        // Это опорный
                        flRNet_Coord = 1;
                        ind_first = ind1;

                        // Фиксируем опорный
                        ClassIRI objS_IRI_RNet6 = new ClassIRI();
                        objS_IRI_RNet6.lst_SP_IRI = new List<SP_IRI>();
                        objS_IRI_RNet6.ID = list_IRI_RNet[ind1].ID;
                        objS_IRI_RNet6.F_IRI = list_IRI_RNet[ind1].F_IRI;
                        objS_IRI_RNet6.Latitude = list_IRI_RNet[ind1].Latitude;
                        objS_IRI_RNet6.Longitude = list_IRI_RNet[ind1].Longitude;
                        objS_IRI_RNet6.lst_SP_IRI = list_IRI_RNet[ind1].lst_SP_IRI;

                        SRNet objSRNet1 = new SRNet();
                        objSRNet1.list_IRI_ID = new List<ClassIRI>();

                        // Добавляем в РС, НО из основного листа пока не убираем
                        NumbRNet1 += 1; // N RS

                        objSRNet1.NRNet = NumbRNet1;
                        objSRNet1.list_IRI_ID.Add(objS_IRI_RNet6);
                        list_SRNet1.Add(objSRNet1);

                        // Выходим из FOR1
                        ind1 = list_IRI_RNet.Count + 1;

                    } // нашли 1-й 


                } // FOR1 (индекс ИРИ, с которым сравниваем остаток массива)
                // -------------------------------------------------------
                // Не нашли (! Здесь сюда Не должно выйти)

                // IF1
                if (flRNet_Coord == 0)
                {
                    // Выходим из WHILE1 (т.е. вообще из формирований РС)
                    ind_first = list_IRI_RNet.Count + 1;
                } // IF1
                // -------------------------------------------------------
                // Нашли опорный - проверяем остаток массива на предмет РС (т.е. SECOND)

                else
                {

                    ind_second = ind_first + 1;

                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Проверка остатка массива на вопрос присоединения к опорному ИРИ

                    // WHILE2
                    while (ind_second < list_IRI_RNet.Count)
                    {

                        // .......................................................                  

                        // Фиксируем Second
                        ClassIRI objS_IRI_RNet7 = new ClassIRI();
                        objS_IRI_RNet7.lst_SP_IRI = new List<SP_IRI>();
                        objS_IRI_RNet7.ID = list_IRI_RNet[ind_second].ID;
                        objS_IRI_RNet7.F_IRI = list_IRI_RNet[ind_second].F_IRI;
                        objS_IRI_RNet7.Latitude = list_IRI_RNet[ind_second].Latitude;
                        objS_IRI_RNet7.Longitude = list_IRI_RNet[ind_second].Longitude;
                        objS_IRI_RNet7.lst_SP_IRI = list_IRI_RNet[ind_second].lst_SP_IRI;

                        // опорный с координатами, second - с координатами
                        if (
                           (NumbRNet1 != 0) &&
                           (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0].Latitude != 0) &&  // oporn
                           (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0].Longitude != 0) &&
                           (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0].Latitude != -1) &&  // oporn
                           (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0].Longitude != -1) &&
                           (objS_IRI_RNet7.Latitude != 0) &&
                           (objS_IRI_RNet7.Longitude != 0) &&
                           (objS_IRI_RNet7.Latitude != -1) &&
                           (objS_IRI_RNet7.Longitude != -1)
                           )
                        {
                            ind_second += 1;
                            goto PPP; // propusk
                        }

                        fp = 0;
                        // опорный с координатами, second - без
                        if (
                           (NumbRNet1 != 0) &&
                           (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0].Latitude != 0) &&  // oporn
                           (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0].Longitude != 0) &&
                           (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0].Latitude != -1) &&  // oporn
                           (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0].Longitude != -1) &&
                           (
                           (objS_IRI_RNet7.Latitude == 0) ||
                           (objS_IRI_RNet7.Longitude == 0) ||
                           (objS_IRI_RNet7.Latitude == -1) ||
                           (objS_IRI_RNet7.Longitude == -1)
                           )
                          )
                        {
                            fp = 1;
                            if (NumbRNet1 != 0)
                            {
                                dff = Math.Abs(list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0].F_IRI - objS_IRI_RNet7.F_IRI);
                                fcompp = ComparePeleng(list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0], objS_IRI_RNet7, dPel);
                            }
                        }

                        // опорный без координат, second - с координатами
                        else if (
                                (NumbRNet1 != 0) &&
                                (
                                 (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0].Latitude == 0) ||  // oporn
                                 (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0].Longitude == 0) ||
                                 (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0].Latitude != -1) ||  // oporn
                                 (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0].Longitude != -1)
                                ) &&
                                (objS_IRI_RNet7.Latitude != 0) &&
                                (objS_IRI_RNet7.Longitude != 0) &&
                                (objS_IRI_RNet7.Latitude != -1) &&
                                (objS_IRI_RNet7.Longitude != -1)
                                )
                        {
                            fp = 1;
                            if (NumbRNet1 != 0)
                            {
                                dff = Math.Abs(list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0].F_IRI - objS_IRI_RNet7.F_IRI);
                                fcompp = ComparePeleng(list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0], objS_IRI_RNet7, dPel);
                            }
                        }

                        // oporn+second NO coordinates
                        else
                        {
                            if (NumbRNet1 != 0)
                            {
                                dff = Math.Abs(list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0].F_IRI - objS_IRI_RNet7.F_IRI);
                                fcompp = ComparePeleng(list_SRNet1[NumbRNet1 - 1].list_IRI_ID[0], objS_IRI_RNet7, dPel);
                            }
                        }

                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Подходит для РС

                        // IF3
                        if (
                           ((fp == 0) && (dff < dF) && (fcompp == -1)) ||
                           ((fp == 1) && (dff < dF) && (fcompp == -1))
                           )
                        {
                            // Формируем РС
                            // IF4  (oporn+second)
                            if (fl_first_Net == 0)
                            {
                                fl_first_Net = 1;

                                // Добавляем в РС (!!! опорный уже добавлен)
                                if (NumbRNet1 != 0)
                                {
                                    list_SRNet1[NumbRNet1 - 1].list_IRI_ID.Add(objS_IRI_RNet7);
                                }

                                // Убрать этот ИРИ из основного списка
                                list_IRI_RNet.RemoveAt(ind_second);
                                // Убрать опорный
                                list_IRI_RNet.RemoveAt(ind_first);
                                ind_second -= 1; // Т.к. убрали сразу два

                            } // IF4 Формируем РС

                            // Добавляем ИРИ к РС 
                            // !!! Здесь SECOND - это не первый SECOND после опорного
                            else // On IF4
                            {
                                // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                // Проверяем, не расположен ли он близко по пеленгу к уже имеющимся в РС

                                fl_tmp = 0;

                                if (NumbRNet1 != 0)
                                {
                                    // FOR33 IRI in RSi
                                    for (i_tmp = 0; i_tmp < list_SRNet1[NumbRNet1 - 1].list_IRI_ID.Count; i_tmp++)
                                    {
                                        fp_1 = 0;

                                        // IRIi с координатами, second с координатами
                                        if (
                                            (NumbRNet1 != 0) &&
                                            (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Latitude != 0) &&  // oporn
                                            (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Longitude != 0) &&
                                            (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Latitude != -1) &&  // oporn
                                            (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Longitude != -1) &&
                                            (objS_IRI_RNet7.Latitude != 0) &&
                                            (objS_IRI_RNet7.Longitude != 0) &&
                                            (objS_IRI_RNet7.Latitude != -1) &&
                                            (objS_IRI_RNet7.Longitude != -1)
                                            )
                                        {
                                            fp_1 = 2;
                                        }

                                        // IRIi с координатами, second - без
                                        else if (
                                                  (NumbRNet1 != 0) &&
                                                  (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Latitude != 0) &&  // oporn
                                                  (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Longitude != 0) &&
                                                  (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Latitude != -1) &&  // oporn
                                                  (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Longitude != -1) &&
                                                  (
                                                  (objS_IRI_RNet7.Latitude == 0) ||
                                                  (objS_IRI_RNet7.Longitude == 0) ||
                                                  (objS_IRI_RNet7.Latitude == -1) ||
                                                  (objS_IRI_RNet7.Longitude == -1)
                                                  )
                                                 )
                                        {
                                            fp_1 = 1;
                                            if (NumbRNet1 != 0)
                                            {
                                                fcompp1 = ComparePeleng(list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp], objS_IRI_RNet7, dPel);
                                            }
                                        }

                                        // IRIi без координат second с координатами
                                        else if (
                                                   (NumbRNet1 != 0) &&
                                                   (
                                                    (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Latitude == 0) ||  // oporn
                                                    (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Longitude == 0) ||
                                                    (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Latitude != -1) ||  // oporn
                                                    (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Longitude != -1)
                                                   ) &&
                                                   (objS_IRI_RNet7.Latitude != 0) &&
                                                   (objS_IRI_RNet7.Longitude != 0) &&
                                                   (objS_IRI_RNet7.Latitude != -1) &&
                                                   (objS_IRI_RNet7.Longitude != -1)
                                               )
                                        {
                                            fp_1 = 1;

                                            if (NumbRNet1 != 0)
                                            {
                                                fcompp1 = ComparePeleng(list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp], objS_IRI_RNet7, dPel);
                                            }
                                        }

                                        // IRIi без координат second без координат
                                        else
                                        {
                                            if (NumbRNet1 != 0)
                                            {
                                                fcompp1 = ComparePeleng(list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp], objS_IRI_RNet7, dPel);
                                            }
                                        }

                                        // Не подходят в РС
                                        if (
                                                (fp_1 == 2) ||
                                                ((fp_1 == 0) && (fcompp1 == 1)) ||
                                                ((fp_1 == 1) && (fcompp1 == 1))
                                          )
                                        {
                                            fl_tmp = 1;
                                            if (NumbRNet1 != 0)
                                            {
                                                i_tmp = list_SRNet1[NumbRNet1 - 1].list_IRI_ID.Count + 1; // Out fromFOR33 (IRI in RSi)
                                            }
                                        }

                                    } // FOR33

                                } // NumbRNet1 != 0
                                // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                // Добавляем SECOND в РС

                                // IF6
                                if (fl_tmp == 0)
                                {
                                    // Добавляем в РС (!!! опорный уже добавлен)
                                    if (NumbRNet1 != 0)
                                    {
                                        list_SRNet1[NumbRNet1 - 1].list_IRI_ID.Add(objS_IRI_RNet7);
                                    }

                                    // Убрать этот ИРИ из основного списка
                                    list_IRI_RNet.RemoveAt(ind_second);

                                } // IF6
                                // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                // Если этот ИРИ стоял близко к какому-то из ИРИ в этой РС

                                else  // On IF6
                                {
                                    // Убрать этот ИРИ из основного списка
                                    list_IRI_RNet.RemoveAt(ind_second);

                                }
                                // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                            } // else onIF4


                        } // IF3 RNet Добавляем ИРИ к РС
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Не подходит для РС - пропускаем

                        else // On IF3
                        {
                            if (
                               ((fp == 0) && (dff < dF) && (fcompp == 1)) ||
                               ((fp == 1) && (dff < dF) && (fcompp == 1))
                              )
                            {
                                // Убрать этот ИРИ из основного списка
                                list_IRI_RNet.RemoveAt(ind_second);
                            }
                            else
                                ind_second += 1;
                        }
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        // .......................................................                  


                        PPP:
                        ind_second = ind_second;

                    } // WHILE2 (проверка остатка массива)

                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Проверяем, сформировалась ли РС
                    // РС сформировалась, т.е. опорный убрали -> переходим к следующему, но индекс тот же
                    // т.к. массив сдвинулся

                    // РС не сформировалась -> Переходим к следующему опорному
                    if (fl_first_Net == 0)
                    {
                        ind_first += 1;
                        flRNet_Coord = 0;
                        if (NumbRNet1 != 0)
                        {
                            list_SRNet1.RemoveAt(NumbRNet1 - 1);
                            NumbRNet1 -= 1;
                        }
                        ind_second = ind_first + 1;
                        fl_first_Net = 0; // Словлена РС

                    } // РС не сформировалась

                    else
                    {
                        flRNet_Coord = 0;
                        ind_second = ind_first + 1;
                        fl_first_Net = 0; // Словлена РС
                    }
                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""


                } // else po IF1 (был опорный ИРИ с координатами)
                // -------------------------------------------------------


            } // while1(ind_first<list_IRI_RNet.Count)
            // ------------------------------------------------------------

            // 22222222222222222222222222222222222222222222222222222222222

            // ****************************************************************************** RNet_Pel

            // ***************************************************************************************
            // Формирование выходного листа
            // list_SRNet+list_SRNet1

            int nmm = 0;

            nmm = list_SRNet.Count + 1;

            ind1 = list_SRNet1.Count;
            var vv = list_SRNet1.ToArray();

            for (i_tmp = 0; i_tmp < ind1; i_tmp++)
            {
                vv[i_tmp].NRNet = nmm;
                list_SRNet.Add(vv[i_tmp]);
                nmm += 1;
            }
            // ***************************************************************************************


            return list_SRNet;

        } // P/P Organization_RNet
          // *************************************************************** RadioNets(RadioDirections)


        // Сравнение 2-ух ИРИ по пеленгам **********************************************************
        // 1 -> пеленги совпадают
        // -1 -> нет
        // -2 -> у obj2 вообще нет пеленгов

        public static int ComparePeleng(
                                         ClassIRI obj1,
                                         ClassIRI obj2,
                                         double dPel
                                       )
        {
            int index = -1;
            int ind1 = 0;
            int flsovp = -1;
            int flsp = 0;

            if ((TestPeleng(obj2) == -1) || (TestPeleng(obj1) == -1))
                return -2;

            //  FOR1 по СП в obj1
            for (ind1 = 0; ind1 < obj1.lst_SP_IRI.Count; ind1++)
            {
                // В obj2 ищем СП с таким же номером
                index = obj2.lst_SP_IRI.FindIndex(x => (x.N_SP == obj1.lst_SP_IRI[ind1].N_SP));
                if (index >= 0)
                {
                    flsp = 1;
                    // Совпали
                    if ((obj1.lst_SP_IRI[ind1].Bearing != -1) &&
                            (obj2.lst_SP_IRI[index].Bearing != -1) &&
                            (Math.Abs(obj1.lst_SP_IRI[ind1].Bearing - obj2.lst_SP_IRI[index].Bearing) < dPel)
                      )
                    {
                        flsovp = 1;
                        ind1 = obj1.lst_SP_IRI.Count + 1; // Выход из FOR
                    }

                } // index>=0

            } // FOR1

            if (flsp == 0)
                flsovp = -2;

            return flsovp;
        }
        // ********************************************************** Сравнение 2-ух ИРИ по пеленгам

        // TEST_PELENG *****************************************************************************
        // Проверка: есть ли у ИРИ хотя бы один пеленг
        // -1 нет
        // 1-есть

        public static int TestPeleng(
                                         ClassIRI obj1
                                       )
        {
            int ind1 = 0;
            int flsovp = -1;

            //  FOR1 по СП в obj1
            for (ind1 = 0; ind1 < obj1.lst_SP_IRI.Count; ind1++)
            {
                // Есть пеленг
                if (
                   (obj1.lst_SP_IRI[ind1].Bearing != -1)
                  )
                {
                    flsovp = 1;
                    ind1 = obj1.lst_SP_IRI.Count + 1; // Выход из FOR
                }

            } // FOR1

            return flsovp;
        }

        // ***************************************************************************** TEST_PELENG

        // CommunicationCenters ********************************************************************
        /// Формирование узлов связи (XYZ==,F!=)

        public static List<SRNet> Organization_Communication(

                                       // ИРИ
                                       List<TableReconFWS> IRI_RNet_Comm,

                                       // !!! Пока внутри программы dF=3KHz dPel=2grad dD=500m
                                       double dD, // m
                                       double dF //KHz
                                                              )
        {

            // -------------------------------------------------------------------------------------
            int nIRI_all = 0;
            int ind1 = 0;
            int shIRI = 0;
            int shSP = 0;
            int NumbRNet = 0;
            int flRNet_Coord = 0;
            int ind_first = 0;
            int fl_first_Net = 0;
            int ind_second = 0;
            double dx = 0;
            double dy = 0;
            double D = 0;
            int i_tmp = 0;
            int fl_tmp = 0;
            // -------------------------------------------------------------------------------------
            // Кол-во ИРИ

            nIRI_all = IRI_RNet_Comm.Count;
            // -------------------------------------------------------------------------------------

            // Основной лист ИРИ
            List<ClassIRI> list_IRI_RNet = new List<ClassIRI>();
            // Лист сформированных УС
            List<SRNet> list_SRNet = new List<SRNet>();

            //S_IRI_RNet objS_IRI_RNet = new S_IRI_RNet();
            //S_IRI_RNet objS_IRI_RNet2 = new S_IRI_RNet();
            //S_IRI_RNet objS_IRI = new S_IRI_RNet();
            //SRNet objSRNet = new SRNet();
            // -------------------------------------------------------------------------------------

            dD = 500; // m
            dF = 3;   // KHz
            // -------------------------------------------------------------------------------------

            // IRI **********************************************************************************
            // Формирование list_IRI_RNet

            double levelmin = -140;
            double levelmax = 0;

            // FOR**
            for (shIRI = 0; shIRI < nIRI_all; shIRI++)
            {

                // -----------------------------------------------------------------------------------
                ClassIRI objS_IRI_RNet = new ClassIRI();
                objS_IRI_RNet.lst_SP_IRI = new List<SP_IRI>();
                // -----------------------------------------------------------------------------------
                // ID

                objS_IRI_RNet.ID = IRI_RNet_Comm[shIRI].Id;
                // -----------------------------------------------------------------------------------
                // Freq

                objS_IRI_RNet.F_IRI = (double)IRI_RNet_Comm[shIRI].FreqKHz;
                // -----------------------------------------------------------------------------------
                // Coordinates

                objS_IRI_RNet.Latitude = IRI_RNet_Comm[shIRI].Coordinates.Latitude;
                objS_IRI_RNet.Longitude = IRI_RNet_Comm[shIRI].Coordinates.Longitude;
                // -----------------------------------------------------------------------------------

                // lst_SP_IRI >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // Составляем список СП, которые видят этот ИРИ

                // FOR12
                for (shSP = 0; shSP < IRI_RNet_Comm[shIRI].ListJamDirect.Count; shSP++)
                {
                    // ..............................................................................
                    SP_IRI objSP_IRI = new SP_IRI();
                    // ..............................................................................

                    // Эта СП видит этот ИРИ
                    if ((IRI_RNet_Comm[shIRI].ListJamDirect[shSP].JamDirect.Level >= levelmin) &&
                        (IRI_RNet_Comm[shIRI].ListJamDirect[shSP].JamDirect.Level <= levelmax))
                    {
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        // Номер СП

                        objSP_IRI.N_SP = IRI_RNet_Comm[shIRI].ListJamDirect[shSP].JamDirect.NumberASP;
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        // Пеленг от этой станции/-1

                        objSP_IRI.Bearing = IRI_RNet_Comm[shIRI].ListJamDirect[shSP].JamDirect.Bearing;
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        objS_IRI_RNet.lst_SP_IRI.Add(objSP_IRI);
                        // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    } // IF(Эта СП видит этот ИРИ)
                    // ..............................................................................

                } // FOR12 -> по списку СП в этом ИРИ

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> lst_SP_IRI

                list_IRI_RNet.Add(objS_IRI_RNet);

            } // FOR** -> IRI
              // ********************************************************************************** IRI

            // YS_XY **********************************************************************************
            // Выделение YS по ИРИ с координатами
            // (F!=; XY=)

            // ------------------------------------------------------------

            flRNet_Coord = 0;  // Нашли опорный
            ind_first = 0;     // Индекс опорного ИРИ
            ind_second = 0;    // Следующий за опорным
            fl_first_Net = 0;  // Словлена РС (т.е кроме опорного, нашли второй)

            // WHILE1 (индекс ИРИ, с которым сравниваем остаток массива)
            // Опорный еще в пределах листа
            while (ind_first < list_IRI_RNet.Count)
            {
                // -------------------------------------------------------
                // Для поиска опорного ири в YS

                // FOR1
                for (ind1 = ind_first; ind1 < list_IRI_RNet.Count; ind1++)
                {
                    if (
                        (list_IRI_RNet[ind1].Latitude != 0) &&
                        (list_IRI_RNet[ind1].Longitude != 0) &&
                        (list_IRI_RNet[ind1].Latitude != -1) &&
                        (list_IRI_RNet[ind1].Longitude != -1) &&
                        (flRNet_Coord == 0)
                        )
                    {
                        // Это опорный
                        flRNet_Coord = 1;
                        ind_first = ind1;

                        // Фиксируем опорный
                        ClassIRI objS_IRI_RNet6 = new ClassIRI();
                        objS_IRI_RNet6.lst_SP_IRI = new List<SP_IRI>();
                        objS_IRI_RNet6.ID = list_IRI_RNet[ind1].ID;
                        objS_IRI_RNet6.F_IRI = list_IRI_RNet[ind1].F_IRI;
                        objS_IRI_RNet6.Latitude = list_IRI_RNet[ind1].Latitude;
                        objS_IRI_RNet6.Longitude = list_IRI_RNet[ind1].Longitude;
                        objS_IRI_RNet6.lst_SP_IRI = list_IRI_RNet[ind1].lst_SP_IRI;

                        SRNet objSRNet = new SRNet();
                        objSRNet.list_IRI_ID = new List<ClassIRI>();

                        // Добавляем в РС, НО из основного листа пока не убираем
                        NumbRNet += 1; // N RS

                        objSRNet.NRNet = NumbRNet;
                        objSRNet.list_IRI_ID.Add(objS_IRI_RNet6);
                        list_SRNet.Add(objSRNet);

                        // Выходим из FOR1
                        ind1 = list_IRI_RNet.Count + 1;
                    } // нашли 1-й с координатами


                } // FOR1 (индекс ИРИ, с которым сравниваем остаток массива)
                // -------------------------------------------------------
                // Не нашли ни одного с координатами-идем дальше

                // IF1
                if (flRNet_Coord == 0)
                {
                    // Выходим из WHILE1
                    ind_first = list_IRI_RNet.Count + 1;
                } // IF1
                // -------------------------------------------------------
                // Нашли с координатами - проверяем остаток массива на предмет YS

                // Else on IF1 (нашли опорный)
                else
                {

                    ind_second = ind_first + 1;

                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Проверка остатка массива на вопрос присоединения к опорному ИРИ

                    // WHILE2
                    while (ind_second < list_IRI_RNet.Count)
                    {

                        // .......................................................                  
                        // есть координаты

                        // IF2 есть координаты
                        if (
                            (list_IRI_RNet[ind_second].Latitude != 0) &&
                            (list_IRI_RNet[ind_second].Longitude != 0) &&
                            (list_IRI_RNet[ind_second].Latitude != -1) &&
                            (list_IRI_RNet[ind_second].Longitude != -1)
                            )
                        {

                            // '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                            // Фиксируем Second
                            ClassIRI objS_IRI_RNet2 = new ClassIRI();
                            objS_IRI_RNet2.lst_SP_IRI = new List<SP_IRI>();
                            objS_IRI_RNet2.ID = list_IRI_RNet[ind_second].ID;
                            objS_IRI_RNet2.F_IRI = list_IRI_RNet[ind_second].F_IRI;
                            objS_IRI_RNet2.Latitude = list_IRI_RNet[ind_second].Latitude;
                            objS_IRI_RNet2.Longitude = list_IRI_RNet[ind_second].Longitude;
                            objS_IRI_RNet2.lst_SP_IRI = list_IRI_RNet[ind_second].lst_SP_IRI;

                            if (fl_first_Net == 0)
                            {
                                D = ClassBearing.f_D_2Points(
                                                            list_IRI_RNet[ind_first].Latitude,
                                                            list_IRI_RNet[ind_first].Longitude,
                                                            objS_IRI_RNet2.Latitude,
                                                            objS_IRI_RNet2.Longitude,
                                                            1);
                            }
                            else
                            {
                                if (NumbRNet != 0)
                                {
                                    D = ClassBearing.f_D_2Points(
                                                                list_SRNet[NumbRNet - 1].list_IRI_ID[0].Latitude,
                                                                list_SRNet[NumbRNet - 1].list_IRI_ID[0].Longitude,
                                                                objS_IRI_RNet2.Latitude,
                                                                objS_IRI_RNet2.Longitude,
                                                                1);
                                }
                            }

                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Подходит для УС

                            // IF3
                            if (
                                ((fl_first_Net == 0) && (D < dD) && (Math.Abs(list_IRI_RNet[ind_first].F_IRI - objS_IRI_RNet2.F_IRI) >= dF)) ||
                                ((NumbRNet != 0) && (fl_first_Net == 1) && (D < dD) && (Math.Abs(list_SRNet[NumbRNet - 1].list_IRI_ID[0].F_IRI - objS_IRI_RNet2.F_IRI) >= dF))
                               )

                            {

                                // """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                                // 1-й, кроме опорного, в YS

                                // IF4
                                if (fl_first_Net == 0)
                                {
                                    fl_first_Net = 1;

                                    // Добавляем в YS (!!! опорный уже добавлен)
                                    if (NumbRNet != 0)
                                    {
                                        list_SRNet[NumbRNet - 1].list_IRI_ID.Add(objS_IRI_RNet2);
                                    }

                                    // Убрать этот ИРИ из основного списка
                                    list_IRI_RNet.RemoveAt(ind_second);
                                    // Убрать опорный
                                    list_IRI_RNet.RemoveAt(ind_first);
                                    ind_second -= 1; // Т.к. убрали сразу два

                                } // IF4 Формируем YS
                                // """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                                // Добавляем ИРИ к YS

                                else // On IF4
                                {
                                    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    // Проверяем, не == ли он по F к уже имеющимся УС

                                    fl_tmp = 0;

                                    if (NumbRNet != 0)
                                    {
                                        for (i_tmp = 0; i_tmp < list_SRNet[NumbRNet - 1].list_IRI_ID.Count; i_tmp++)
                                        {
                                            if (Math.Abs(list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].F_IRI - objS_IRI_RNet2.F_IRI) < dF)
                                            {
                                                fl_tmp = 1;
                                                i_tmp = list_SRNet[NumbRNet - 1].list_IRI_ID.Count + 1; // Out fromFOR
                                            }
                                        }
                                    }
                                    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                                    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    // Добавляем в РС

                                    // IF6
                                    if (fl_tmp == 0)
                                    {
                                        // Добавляем в РС (!!! опорный уже добавлен)
                                        if (NumbRNet != 0)
                                        {
                                            list_SRNet[NumbRNet - 1].list_IRI_ID.Add(objS_IRI_RNet2);
                                        }

                                        // Убрать этот ИРИ из основного списка
                                        list_IRI_RNet.RemoveAt(ind_second);

                                    } // IF6
                                      // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                      // Если этот ИРИ стоял близко к какому-то из ИРИ в этой РС

                                    else  // On IF6
                                    {
                                        // Убрать этот ИРИ из основного списка
                                        list_IRI_RNet.RemoveAt(ind_second);

                                    }
                                    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                                } // else onIF4


                            } // IF3 RNet Добавляем ИРИ к YS
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Не подходит для YS - пропускаем

                            else // On IF3
                            {
                                if (
                                    ((fl_first_Net == 0) && (D <= dD) && (Math.Abs(list_IRI_RNet[ind_first].F_IRI - objS_IRI_RNet2.F_IRI) <= dF)) ||
                                    ((NumbRNet != 0) && (fl_first_Net == 1) && (D <= dD) && (Math.Abs(list_SRNet[NumbRNet - 1].list_IRI_ID[0].F_IRI - objS_IRI_RNet2.F_IRI) <= dF))
                                    )
                                {
                                    // Убрать этот ИРИ из основного списка
                                    list_IRI_RNet.RemoveAt(ind_second);
                                }
                                else
                                    ind_second += 1;

                            } // else on IF3
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                        } // IF2 есть координаты
                        // .......................................................                  
                        // Нет координат- пропускаем

                        else // On IF2
                        {
                            ind_second += 1;
                        }
                        // .......................................................                  

                    } // WHILE2 (проверка остатка массива)

                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Проверяем, сформировалась ли YS
                    // YS сформировалась, т.е. опорный убрали -> переходим к следующему, но индекс тот же
                    // т.к. массив сдвинулся

                    // YS не сформировался -> Переходим к следующему опорному
                    if (fl_first_Net == 0)
                    {
                        ind_first += 1;
                        flRNet_Coord = 0;
                        if (NumbRNet != 0)
                        {
                            list_SRNet.RemoveAt(NumbRNet - 1);
                        }
                        NumbRNet -= 1;
                        ind_second = ind_first + 1;
                        fl_first_Net = 0; // Словлен YS

                    } // YS не сформировался

                    else
                    {
                        flRNet_Coord = 0;
                        ind_second = ind_first + 1;
                        fl_first_Net = 0; // Словлен YS
                    }

                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""

                } // else po IF1 (был опорный ИРИ с координатами)
                // -------------------------------------------------------


            } // while1(ind_first<list_IRI_RNet.Count)
              // ------------------------------------------------------------

            // ********************************************************************************** YS_XY

            // **********************************************************
            // Формирование выходных массивов

            //for (i_tmp = 0; i_tmp < list_S_YS_RNet.Count; i_tmp++)
            //{
            //    list_Comm.Add(list_S_YS_RNet[i_tmp]);
            // }
            // **********************************************************


            return list_SRNet;
        }
        // *********************************************************************** CommunicationCenters


    } // Class
} // Namespace
